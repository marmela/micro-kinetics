import sys
import numpy as np
from scipy import integrate
import copy
from ase.thermochemistry import HarmonicThermo
'''
    H2O + * = H2O*
    H2O* + * = OH* + H*
    OH* + * = O* + H*
    OH* + OH* = H2O* + O*
    2H* = H2 + 2*
    CO + * = CO* 
    CO* + O* = CO2* + * 
    CO2* = CO2 + *
    CO* + OH* = COOH* + *
    COOH* + * = CO2* + H*
    CO* + H* = HCO* + *
    HCO* + O* = HCOO* + *
    HCOO* + * = CO2* + H*
'''
class MicroKinetic():

  def __init__(self, dH, dHa,vibs, entropies, P0, T, theta0 = [0., 0., 0., 0., 0., 0., 0., 0. ,0.] ,
              reactor_volume = 5000, cat_mass = 50e-5, bet = 50e4,                        
              t = 1, steps=100000, Xrc = True):
    # CONTROLS 
    self.steps = steps
    self.tspan = np.linspace(0, t, self.steps)
    self.make_xrc_or_not = Xrc
    self.entropies = entropies
    # Conditions
    self.P0 = copy.copy(P0)
    self.T = T #K
    self.PCO2 = P0[0] #bar
    self.PH2 = P0[1] #bar
    self.PH2O = P0[2] #bar
    self.PCO = P0[3] #bar
    # # Constants

    J2eV = 6.24e18 
    Na = 6.023e23 
    self.h = 6.626e-34 * J2eV
    self.kb = 1.38e-23 * J2eV
    self.kbT = self.kb*self.T
    self.k = 1.38064852e-23 # J/K
    self.RT = 83.144598*T # (cm3*bar/mol*K) *K
    
    # IN THIS VERSION, THE  RATE OF ADSORPTION IS TAKEN
    # FROM STICKING COEFFICIENTS FOR WATER, CO, CO2 and H2
    # area of an adsorption site for fcc111 (a,b = cell vectors)
    # A = np.linalg.norm(np.cross(a,b))
    # N0 = sites/A , sitesH2O = n_tops=4, sites_CO = n_fcc=2

    n_tops = 2 # number of sites per unit cell
    n_hollows = 4 # -""-
    Acell = 6.3e-16 #cm^2

    self.reactor_volume = reactor_volume #cm3
    self.cat_mass = cat_mass#g
    molar_mass = 102.9055 #g/mol of catalyst
    n_bet = bet * molar_mass #cm2/mol
    n_cat = self.cat_mass / molar_mass #mol
    self.total_surface_area = self.cat_mass * bet #cm2

    n_ads_places = 1. / Acell # sites/cm2
    self.n_surf_atoms = self.cat_mass * bet / (Na * Acell)

    #nmax = Na * area * C_surf_atoms # atoms on surface --> theta = 1 corresponds to nmax

    # For kinetic gas theory
    # r = P/(sqrt(2*pi*m*k*T)) S(theta,T)
    ads_prefactor = 8.51057e13 #1/s  #bar/(1/cm2*sqrt(boltzmannconstant*K*g))

    self.CO_prefactor = ads_prefactor/(n_ads_places*np.sqrt(np.pi*(28.01/Na)*T))
    self.H2O_prefactor = ads_prefactor/(n_ads_places*np.sqrt(np.pi*(18.016/Na)*T))
    self.H2_prefactor = ads_prefactor/(n_ads_places*np.sqrt(np.pi*(2.016/Na)*T))
    self.CO2_prefactor = ads_prefactor/(n_ads_places*np.sqrt(np.pi*(44.01/Na)*T))
    print(self.CO_prefactor)
    #print(theta0)
    theta0 = theta0+self.P0
    self.theta0 = theta0
    #print(self.theta0)
    self.dH0 = dH # reaction enthalpies
    self.dHa0 = dHa #reaction barriers
    self.n_reactions = len(self.dH0)
    self.vibs = vibs
    self.dG0, self.dGa0 = self.get_gibbs_energies()  # reaction (Gibbs) energies & barriers in eV

  def get_gibbs_energies(self):
    
    ####################
    # Define reaction (Gibbs) energies and barriers in eV
    '''
      H2O + * = H2O*
      H2O* + * = OH* + H*
      OH* + * = O* + H*
      OH* + OH* = H2O* + O*
      2H* = H2 + 2* # 2nd order Langmuir http://www.sciencedirect.com/science/article/pii/0039602879901468
      CO + * = CO*
      CO* + O* = CO2* + * 
      CO2* = CO2 + *
      CO* + OH* = COOH* + *
      COOH* + * = CO2* + H*
      CO* + H* = HCO* + *
      HCO* + O* = HCOO* + *
      HCOO* + * = CO2* + H*
    '''
    ######################################################

    self.vibs_eV =  [i*0.001 for i in self.vibs]
    self.S = [0]*(len(self.vibs)+4) 

    # makes array of entropies for every adsorbate, TS, and gasphase species

    for i in range(0, len(self.S)):
       if i < 23 and len(self.vibs[i]) > 1:
           thermo = HarmonicThermo(vib_energies = self.vibs_eV[i])
           self.S[i]=thermo.get_entropy(self.T)*self.T
      
       elif i < 23 and len(self.vibs[i]) == 1:
            self.S[i] = 0
       elif i ==23:
            self.S[i] = self.entropies['H2O']*self.T
       elif i ==24:
            self.S[i] = self.entropies['H2']*self.T
       elif i ==25:
            self.S[i] = self.entropies['CO']*self.T
       elif i ==26:
            self.S[i] = self.entropies['CO2']*self.T

    self.dG = [0]*len(self.dH0)
    self.dGa = [0]*len(self.dHa0)

    self.dG[0] = self.dH0[0] + (self.S[23]-self.S[1])          # H2O + * = H2O*
    self.dG[1] = self.dH0[1] + (self.S[1]-self.S[2]-self.S[3]) # H2O* + * = OH* +H*
    self.dG[2] = self.dH0[2] + (self.S[2]-self.S[4]-self.S[3]) # OH* + * = O* + H*
    self.dG[3] = self.dH0[3] + (self.S[2]+self.S[2]-self.S[1]-self.S[4]) # OH* + OH* = H2O* + O*
    self.dG[4] = self.dH0[4] + (self.S[3]+self.S[3]-self.S[24]) # 2H* = H2 + 2*
    self.dG[5] = self.dH0[5] + (self.S[25]-self.S[5])          # CO + * = CO* 
    self.dG[6] = self.dH0[6] + (self.S[5]+self.S[4]-self.S[6]) # CO* + O* = CO2* + * 
    self.dG[7] = self.dH0[7] + (self.S[6]-self.S[26])          # CO2* = CO2 + *
    self.dG[8] = self.dH0[8] + (self.S[5]+self.S[2]-self.S[7]) # CO* + OH* = COOH* + *
    self.dG[9] = self.dH0[9] + (self.S[7]-self.S[6]-self.S[3]) # COOH* + * = CO2* + H*
    self.dG[10] = self.dH0[10] + (self.S[5]+self.S[3]-self.S[8]) # CO* + H* = HCO* + *
    self.dG[11] = self.dH0[11] + (self.S[8]+self.S[4]-self.S[9]) # HCO* + O* = HCOO* + *
    self.dG[12] = self.dH0[12] + (self.S[9]-self.S[6]-self.S[3]) # HCOO* + * = CO2* + H*
    
    if self.dG[0] <= 0:
       self.dGa[0] = self.dHa0[0]                           # H2O + * = H2O*
    if self.dG[0] > 0:  
      self.dGa[0] = self.dG[0]    
    self.dGa[1] = self.dHa0[1] + (self.S[1]-self.S[11]) # H2O* + * = OH* +H*
    self.dGa[2] = self.dHa0[2] + (self.S[2]-self.S[12]) # OH* + * = O* + H*
    self.dGa[3] = self.dG[3]                            # OH* + OH* = H2O* + O*
    if self.dG[4] <= 0:
      self.dGa[4] = 0.000001
    if self.dG[4] > 0:  
      self.dGa[4] = self.dG[4]                           # 2H* = H2 + 2*
    if self.dG[5] <= 0:
      self.dGa[5] = self.dHa0[5]                           # CO + * = CO* 
    if self.dG[5] > 0:  
      self.dGa[5] = self.dG[5] 
    self.dGa[6] = self.dHa0[6] + (self.S[5]+self.S[4]-self.S[16]) # CO* + O* = CO2* + * 
    if self.dG[7] <= 0: 
      self.dGa[7] = 0.000001                            # CO2* = CO2 + *
    if self.dG[7] > 0:  
      self.dGa[7] = self.dG[7] 
    self.dGa[8] = self.dHa0[8] + (self.S[5]+self.S[2]-self.S[18]) # CO* + OH* = COOH* + *
    self.dGa[9] = self.dHa0[9] + (self.S[7]-self.S[19]) # COOH* + * = CO2* + H*
    self.dGa[10] = self.dHa0[10] + (self.S[5]+self.S[3]-self.S[20]) # CO* + H* = HCO* + *
    self.dGa[11] = self.dHa0[11] + (self.S[8]+self.S[4]-self.S[22]) # HCO* + O* = HCOO* + *
    self.dGa[12] = self.dHa0[12] + (self.S[9]-self.S[23]) # HCOO* + * = CO2* + H*
  
    for i in range(0, len(self.dG)):
       print( 'dG and dGa  of step %s: %s & %s' % (i,self.dG[i],self.dGa[i]))
    
    return(self.dG, self.dGa)
  
  def update_rate_constants(self, dG, dGa, return_k = False):
    self.K = []

    for a in range(self.n_reactions):
       self.K.append(np.exp(-dG[a]/self.kbT))

    '''Compute the rate constants based
    on (Gibbs) reaction energies and
    barriers,
    returns the rate constants'''

    self.kf = self.n_reactions*[0]
    self.kr = self.n_reactions*[0]

    for i in range(self.n_reactions):
      self.kf[i] = self.kbT/self.h*np.exp(-dGa[i]/self.kbT) 
      # thermodynamic consistency
      self.kr[i] = self.kf[i] / self.K[i]

    self.kf[0] = 0.54 * self.H2O_prefactor
    self.kr[0] = self.kf[0]/self.K[0]
    # CO
    self.kf[5] = self.CO_prefactor  
    self.kr[5] = self.kf[5]/self.K[5]
    #CO2
    self.kr[7] = self.CO2_prefactor 
    self.kf[7] = self.K[7]*self.kr[7]
    #H2
    self.kr[4] = self.H2_prefactor
    self.kf[4] = self.K[4]*self.kr[4]

    if return_k:
      return (self.kf,self.kr)


  def get_odes_for_ode_solver(self, t, theta):
    '''
    H2O + * = H2O*
    H2O* + * = OH* + H*
    OH* + * = O* + H*
    OH* + OH* = H2O* + O*
    2H* = H2 + 2*
    CO + * = CO* 
    CO* + O* = CO2* + * 
    CO2* = CO2 + *
    CO* + OH* = COOH* + *
    COOH* + * = CO2* + H*
    CO* + H* = HCO* + *
    HCO* + O* = HCOO* + *
    HCOO* + * = CO2* + H*
    '''
    theta = np.clip(theta,a_min = 0., a_max = np.max(theta))
    
    tH2O = theta[0]
    nH2O_s = self.n_surf_atoms * tH2O
    
    tH = theta[1]
    nH_s = tH * self.n_surf_atoms
    
    tOH = theta[2]
    tO = theta[3]
    
    tCO = theta[4]
    nCO_s = tCO * self.n_surf_atoms

    tCO2 = theta[5]
    nCO2_s = tCO2 * self.n_surf_atoms
    
    
    tCOOH = theta[6]
    tHCO = theta[7]
    tHCOO = theta[8]
    
    PCO2_t = theta[9]
    PH2_t = theta[10]
    PH2O_t = theta[11] 
    PCO_t = theta[12] 
    
    #nCO2_g = theta[9] * reactor_volume/RT
    #nH2_g = theta[10] * reactor_volume/RT
    #nH2O_g = theta[11] * reactor_volume/RT
    #nCO_g = theta[12] * reactor_volume/RT 
    tstar = 1. - tHCO - tCO - tO - tHCOO - tH2O - tOH - tH - tCO2 - tCOOH

    nstar = tstar * self.n_surf_atoms

    rate = self.n_reactions*[0]

    # modified adsorption rates
    # H2O
    self.kf[0] = 0.54 * self.H2O_prefactor
    self.kr[0] = self.kf[0]/self.K[0]
    # CO
    self.kf[5] = (0.875*tstar**2 - 1.725*tstar + 0.85) * self.CO_prefactor  
    self.kr[5] = self.kf[5]/self.K[5]
    #CO2
    self.kr[7] = 1. * self.CO2_prefactor 
    self.kf[7] = self.K[7]*self.kr[7]
    #H2
    self.kr[4] = 1. * self.H2_prefactor
    self.kf[4] = self.K[4]*self.kr[4]

    #units 1/s
    rate[0] = self.kf[0]* PH2O_t * tstar - self.kr[0]*tH2O # should tstar appear for H2O?
    rate[1] = self.kf[1]*tH2O*tstar - self.kr[1]*tOH*tH
    rate[2] = self.kf[2]*tOH*tstar - self.kr[2]*tO*tH
    rate[3] = self.kf[3]*tOH**2 - self.kr[3]*tO*tH2O
    rate[4] = self.kf[4]*tH**2 - self.kr[4]*PH2_t*tstar**2 # H2
    rate[5] = self.kf[5]*PCO_t*tstar - self.kr[5]*tCO # should tstar appear for CO?
    rate[6] = self.kf[6]*tCO*tO - self.kr[6]*tCO2*tstar
    rate[7] = self.kf[7]*tCO2 - self.kr[7]*PCO2_t*tstar #CO2
    rate[8] = self.kf[8]*tOH*tCO - self.kr[8]*tCOOH*tstar
    rate[9] = self.kf[9]*tCOOH*tstar - self.kr[9]*tCO2*tH
    rate[10] = self.kf[10]*tCO*tH - self.kr[10]*tHCO*tstar
    rate[11] = self.kf[11]*tHCO*tO - self.kr[11]*tHCOO*tstar
    rate[12] = self.kf[12]*tHCOO*tstar - self.kr[12]*tCO2*tH

    n = len(theta)
    dthetadt = np.zeros((n,1))

    dthetadt[0] = rate[0] - rate[1] + rate[3]
    dthetadt[1] = rate[1] + rate[2] - 2*rate[4] + rate[9] - rate[10] + rate[12]
    dthetadt[2] = rate[1] - rate[2] - 2*rate[3] - rate[8]
    dthetadt[3] = rate[2] + rate[3] - rate[6] - rate[11]
    dthetadt[4] = rate[5] - rate[6] - rate[8] - rate[10]
    dthetadt[5] = rate[6] - rate[7] + rate[9] + rate[12]
    dthetadt[6] = rate[8] - rate[9]
    dthetadt[7] = rate[10] - rate[11]
    dthetadt[8] = rate[11] - rate[12]
    
    # pressures --> need to modified to account for changes in pressure
    # theta_i = n_i/nmax --> n_i = theta_i*nmax
    #CO2 # dP/dt = des - ads
    dthetadt[9] = rate[7] * self.n_surf_atoms * self.RT / self.reactor_volume
    #dthetadt[9] = (RT *n_surf_atoms/ reactor_volume) * rate[7] #PCO2 in bar
    # H2
    dthetadt[10] = rate[4] * self.n_surf_atoms * self.RT / self.reactor_volume
    #dthetadt[10] = (RT*n_surf_atoms /reactor_volume) * rate[4] #PH2 
    
    # H2O
    #dthetadt[11] = -(RT*n_surf_atoms /reactor_volume) * rate[0] #P_H2O
    dthetadt[11] = -rate[0] *  self.n_surf_atoms * self.RT / self.reactor_volume
    #dthetadt[11] = (kf[0] * nH2O_g * n_surf_atoms - kr[0] * nH2O_s)* RT/reactor_volume
    #CO
    #dthetadt[12] = -(RT*n_surf_atoms /reactor_volume) * rate[5] #P_CO
    dthetadt[12] = -rate[5] *  self.n_surf_atoms * self.RT / self.reactor_volume

    return dthetadt

  def get_rates(self, c, average_over=1):

    theta.clip(min=1e-32)
    
    tH2O = sum(c[-average_over:,0]) / average_over
    nH2O_s = self.n_surf_atoms * tH2O
    
    tH = sum(c[-average_over:,1])/ average_over
    nH_s = tH * self.n_surf_atoms
    
    tOH = sum(c[-average_over:,2])/ average_over
    tO = sum(c[-average_over:,3])/ average_over
    
    tCO = sum(c[-average_over:,4])/ average_over
    nCO_s = tCO * self.n_surf_atoms  
    
    tCO2 = sum(c[-average_over:,5])/ average_over
    nCO2_s = tCO2 * self.n_surf_atoms
    
    tCOOH = sum(c[-average_over:,6])/ average_over
    tHCO = sum(c[-average_over:,7])/ average_over
    tHCOO = sum(c[-average_over:,8])/ average_over
    
    PCO2_t = sum(c[-average_over:,9])/ average_over
    PH2_t = sum(c[-average_over:,10])/ average_over
    PH2O_t = sum(c[-average_over:,11])/ average_over
    PCO_t = sum(c[-average_over:,12])/ average_over 
    tstar = 1. - tHCO - tCO - tO - tHCOO - tH2O - tOH - tH - tCO2 - tCOOH

    nstar = tstar * self.n_surf_atoms

    '''
    H2O + * = H2O*
    H2O* + * = OH* + H*
    OH* + * = O* + H*
    OH* + OH* = H2O* + O*
    2H* = H2 + 2*
    CO + * = CO* 
    CO* + O* = CO2* + * 
    CO2* = CO2 + *
    CO* + OH* = COOH* + *
    COOH* + * = CO2* + H*
    CO* + H* = HCO* + *
    HCO* + O* = HCOO* + *
    HCOO* + * = CO2* + H*
    '''

    rate = np.zeros(self.n_reactions)

    # modified adsorption rates
    # H2O
    
    #units 1/s
    rate[0] = self.kf[0]* PH2O_t * tstar - self.kr[0]*tH2O # should tstar appear for H2O?
    rate[1] = self.kf[1]*tH2O*tstar - self.kr[1]*tOH*tH
    rate[2] = self.kf[2]*tOH*tstar - self.kr[2]*tO*tH
    rate[3] = self.kf[3]*tOH**2 - self.kr[3]*tO*tH2O
    rate[4] = self.kf[4]*tH**2 - self.kr[4]*PH2_t*tstar**2 # H2
    rate[5] = self.kf[5]*PCO_t*tstar - self.kr[5]*tCO # should tstar appear for CO?
    rate[6] = self.kf[6]*tCO*tO - self.kr[6]*tCO2*tstar
    rate[7] = self.kf[7]*tCO2 - self.kr[7]*PCO2_t*tstar #CO2
    rate[8] = self.kf[8]*tOH*tCO - self.kr[8]*tCOOH*tstar
    rate[9] = self.kf[9]*tCOOH*tstar - self.kr[9]*tCO2*tH
    rate[10] = self.kf[10]*tCO*tH - self.kr[10]*tHCO*tstar
    rate[11] = self.kf[11]*tHCO*tO - self.kr[11]*tHCOO*tstar
    rate[12] = self.kf[12]*tHCOO*tstar - self.kr[12]*tCO2*tH

    return rate, copy.copy(self.kf), copy.copy(self.kr)
  
  def ode_integrator(self):
      r = integrate.ode(self.get_odes_for_ode_solver).set_integrator('vode',nsteps = 1000000000, 
      atol = 1e-15,rtol = 1e-15, order = 4, method='bdf', with_jacobian = True)
      
      t_start = min(self.tspan)
      t_final = max(self.tspan)
      n_steps = len(self.tspan)
      delta_t = (t_final - t_start) / self.steps
      theta0 = copy.copy(self.theta0)
      
      r.set_initial_value(theta0, t_start)

      t = np.zeros((self.steps, 1))
      concentrations = np.zeros((self.steps, len(theta0)))
      t[0] = t_start
      concentrations[0] = copy.copy(theta0)
      k = 1
      #print('Total steps ', n_steps)
      while r.successful() and k < n_steps:
          r.integrate(r.t + delta_t)
          t[k] = r.t
          concentrations[k] = r.y
          k += 1
      #print(kf)
      return t, concentrations

  def solve_microkinetic_model(self, return_tofs = False):
    self.update_rate_constants(self.dG0, self.dGa0)
    self.t0, self.c0 = self.ode_integrator()
    self.tofspan = copy.copy(self.tspan[1:])
    self.TOF_CO2 = (((self.reactor_volume*self.c0[1:,-2])/self.RT)/self.cat_mass) 
    self.TOF_H2 = (((self.reactor_volume*self.c0[1:,-1])/self.RT)/self.cat_mass)  
    self.Y_CO2, self.Y_H2 = self.get_yield()
    self.Xi_CO2, self.Xi_H2, self.CO2_eq, self.H2_eq = self.get_extent()
    if self.make_xrc_or_not:
      self.make_xrc()
    if return_tofs:
      return self.TOF_CO2, self.TOF_H2


  def get_yield(self):
     S_co2 = min(self.P0[3], self.P0[2])
     S_h2 = self.P0[2]
     P_co2 = np.array(self.c0[1:,-4])
     P_h2 = np.array(self.c0[1:,-3])
     Y_co2 = (P_co2/S_co2)*100
     Y_h2 = (P_h2/S_h2)*100
     return Y_co2, Y_h2

  def get_extent(self):
     CO2_t = np.array(self.c0[1:,-4])
     CO2_eq = CO2_t[-1]
     H2_t = np.array(self.c0[1:,-3])
     H2_eq = H2_t[-1] 
     Xi_co2 = (CO2_t/CO2_eq)*100
     Xi_h2 = (H2_t/H2_eq)*100
     return Xi_co2, Xi_h2, CO2_eq, H2_eq

  def find_nearest(self,array,value):
     idx = (np.abs(array-value)).argmin()
     return idx

  def make_xrc(self, x_type = 'thermodynamic', delta = 0.1, average_over = 1):
    # X_rc Analysis
    self.update_rate_constants(self.dG0, self.dGa0, return_k = False)
    Xrc_H2 = [] 
    Xrc_CO2 = []
    # Determine 30% equilib time

    value = 0.3*self.H2_eq
    third = self.find_nearest(self.Xi_H2, value)

  
    # ODE integrator uses kf and kr, not kf0 and kr0
    t, c0 = self.ode_integrator() # one regular mk run
     
    rates0, kf0, kr0= self.get_rates(c0, average_over = average_over) #get array of rates
    r0_CO2 = rates0[7] # rate of CO2 formation 
    r0_H2 = rates0[4] # rate of H2 formation
   
    print('Initial rate of H2 and CO2 formation: %s %s' % (r0_H2, r0_CO2))
    print('Initial rate constants: %s' % kf0)
 #   tofspan = self.tspan[1:]
 #   TOF0_CO2 = ((self.reactor_volume*c0[1:,-2]/self.RT)/self.cat_mass) /tofspan
 #   TOF0_H2 = (((self.reactor_volume*c0[1:,-1])/self.RT)/self.cat_mass) /tofspan
 #   TOF0_CO2 = sum(TOF0_CO2[-average_over:])/average_over
 #   TOF0_H2 = sum(TOF0_H2[-average_over:])/average_over
    
    for i in range(self.n_reactions):
      init_prefactors=[self.H2O_prefactor,self.H2_prefactor,self.CO_prefactor,self.CO2_prefactor]
      if x_type == 'kinetic':
        # H2O
        if i == 0:
          self.H2O_prefactor *= (1.-delta)
        # CO
        if i == 5:
          self.CO_prefactor *= (1.-delta) 
        if i == 7:
          #CO2
          self.CO2_prefactor *= (1.-delta)
        if i == 4:
          #H2
          self.H2_prefactor *= (1.-delta)
        
        dGa_m = copy.copy(self.dGa0)
        dGa_m[i] += delta
        self.update_rate_constants(self.dG0, dGa_m)
	
      elif x_type == 'thermodynamic':
        dG_m = copy.copy(self.dG0)
        dG_m[i] += delta 
        self.update_rate_constants(dG_m, self.dGa0)        

      t, c = self.ode_integrator() # solves odes with modified rate constants
      rates, kf, kr = self.get_rates(c, average_over=average_over) #gets array of modified rates
      
      #print('---------------------')
      #print('TOFs XRC step %d'%i)
      TOF_CO2 = ((self.reactor_volume*c[1:,9]/self.RT)/self.cat_mass) 
      TOF_H2 = (((self.reactor_volume*c[1:,10])/self.RT)/self.cat_mass) 

      TOF_CO2 = sum(TOF_CO2[-average_over:])/average_over
      TOF_H2 = sum(TOF_H2[-average_over:])/average_over

      #print(TOF_CO2)
      #print(TOF_H2)
      r_CO2 = rates[7] # rate of CO2 formation
      r_H2 = rates[4] # rate of H2 formation
      # TOF CONTROL
      #Xrc_H2.append( (kf0[i]/TOF0_H2)* (TOF_H2 - TOF0_H2)/(kf[i]-kf0[i]) )
      #Xrc_CO2.append( (kf0[i]/TOF0_CO2)* (TOF_CO2 - TOF0_CO2)/(kf[i]-kf0[i]) )
      print('---------------------')
      print('Rate of H2 and CO2 formation for current step: %s %s' % (r_H2, r_CO2))
      #Rate control
      if x_type == 'kinetic':
        Xrc_H2.append( (kf0[i]/r0_H2) * ((r_H2 - r0_H2)/(kf[i]-kf0[i])) )
        Xrc_CO2.append( (kf0[i]/r0_CO2) * ((r_CO2 - r0_CO2)/(kf[i]-kf0[i])) )
      elif x_type == 'thermodynamic':
        Xrc_H2.append( (1./r0_H2) * (r_H2 - r0_H2)/( -1./(self.kbT) * (dG_m[i]-self.dG0[i])))
        Xrc_CO2.append( (1./r0_CO2) * (r_CO2 - r0_CO2)/( -1./(self.kbT) * (dG_m[i]-self.dG0[i])))
       
      # reset the original parameters 
      self.H2O_prefactor = init_prefactors[0]
      self.H2_prefactor = init_prefactors[1]
      self.CO_prefactor = init_prefactors[2]
      self.CO2_prefactor = init_prefactors[3]
      self.update_rate_constants(self.dG0, self.dGa0)   

    print('--------------------------------')
    print('Rate XRC')
    print('step   CO2    H2   ')
    print('--------------------------------')
    
    for i in range(0,self.n_reactions):
      print(i, '    ' , np.round(Xrc_CO2[i],3), '    ' , np.round(Xrc_H2[i], 3))
    print('--------------------------------')

  def plot_microkinetics(self):

    import matplotlib.pyplot as plt
    import numpy as np

    tags=['H2O', 'H', 'OH', 'O', 'CO', 'CO2', 'COOH', 'HCO', 'HCOO']
    colours=['#0504aa','#929591','#6dedfd','#8e82fe', #royal blue, grey, bright sky blue, periwinkle
             '#5ca904','#ad8150', #leaf green, puce
           #  '#f97306', '#be0119', '#fe019a',
             '#fb7d07','#be0119', '#fe019a', #pumpkin orange, cherry, barbie pink
             '#929591','#000000'] #grey, black
    linestyles=['-', '-', '-','-', '-','-','-','-','-']

    fig=plt.figure(figsize=(22,10))
    # COVERAGES
    cov=plt.subplot(311) # plot of coverages 
    cov.set_ylabel('Coverage / ML', fontsize=10)
    cov.set_xlabel('$\log$ time / s', fontsize=10)
    
    for i in range(len(tags)):
        cov.plot(np.log10(self.t0[1:]), self.c0[1:,i], linestyle=linestyles[i], c=colours[i], label='%s'%tags[i], linewidth=2)
    
    leg1=cov.legend(loc=2, ncol=1, mode=None, borderaxespad=0.0, bbox_to_anchor=(1.05, 1))
    cov.set_ylim(-0.1,1.1)
    title=plt.title('WGS on Rh/ZrO2 interface at %s K with %s:%s H2O:CO'% (self.T, self.PH2O, self.PCO))
    title.set_position([.5, 1.1])
    
    # YIELD & EXTENT

    Xi=plt.subplot(312) # plot of yield
    Xi.set_ylabel('% Molar extent', fontsize=10)
    Xi.plot(np.log10(self.t0[1:]), self.Xi_CO2, linestyle= '--', c= '#0504aa', label='CO2', linewidth=2) 
    Xi.plot(np.log10(self.t0[1:]), self.Xi_H2, linestyle= '--', c= '#929591', label='H2', linewidth=2) 
    Xi.set_ylim(0,101)
    
    Y = Xi.twinx()
    Y.set_ylabel('% Yield', fontsize=10)
    Y.set_xlabel('$\log$ time / s', fontsize=10)
    Y.plot(np.log10(self.t0[1:]), self.Y_CO2, linestyle= '-', c= '#0504aa', label='CO2', linewidth=2)
    Y.plot(np.log10(self.t0[1:]), self.Y_H2, linestyle= '-', c= '#929591', label='H2', linewidth=2)

    leg2 = Y.legend(loc=2,ncol=1, title= 'Yield', mode=None, borderaxespad=0.0, bbox_to_anchor=(1.05, 1 ))
    legXi = Xi.legend(loc=2, ncol=1, title='Extent', mode=None, borderaxespad=0.0, bbox_to_anchor=(1.05, 0.5 ))

    # TOFS

#    tof=plt.subplot(312) # plot of TOF
#    tof.set_ylabel('TON (mol)', fontsize=10)
#    tof.set_xlabel('t [s]',fontsize=10)
#    tof.plot(np.log10(self.tofspan),self.TOF_H2, linestyle = '--', c = colours[-2], linewidth = 2,label = r'TOF(H2)')
#    tof.plot(np.log10(self.tofspan),self.TOF_CO2, linestyle = '--', c = colours[-1], linewidth = 2,label = r'TOF(CO2)')
#    leg2 = tof.legend(loc=2,ncol=1, mode=None, borderaxespad=0.0, bbox_to_anchor=(1.0005,1 ))

    # PRESSURES
    tags=['CO2', 'H2', 'H2O', 'CO']
    colours=['#0504aa','#929591','#6dedfd','#8e82fe'] #royal blue, grey, bright sky blue, periwinkle
    pres=plt.subplot(313) # plot of pressures 
    pres.set_ylabel('Pressure / bar', fontsize=10)

    pres.set_xlabel('$\log$ time / s', fontsize=10)
    #cov.set_ylim(0,1)

    for i, p in enumerate([9,10,11,12]):
        pres.plot(np.log10(self.t0[1:]), self.c0[1:,p], linestyle=linestyles[i], c=colours[i], label='%s'%tags[i], linewidth=2)
    leg3=pres.legend(loc=2, ncol=1, mode=None, borderaxespad=0.0, bbox_to_anchor=(1.05, 1))


    #title=plt.title('WGS on Rh/ZrO2 interface at %s K and %s bar with %s:%s water:gas'% (T,PH2O+PCO+PCO2+PH2,water,1))
    #title.set_position([.5, 1.1])


    #fname='WGS_Rh-ZrO2_mk_at_%sK_and_%sbar_with_%sto%s_water-gas_ratio.png'% (T,PH2O+PCO+PCO2+PH2,water,1)

    fig.show()
    fig.savefig('%s_%sto%s.png' % (self.T, self.PH2O, self.PCO))
